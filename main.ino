/* You must select Keyboard from the "Tools > USB Type" menu */
#include <Bounce2.h>

int pin0 = 7;
int pin1 = 12;
int pin2 = 14;

int key0 = KEY_Z;
int key1 = KEY_X;
int key2 = KEY_C;

// Create Bounce objects for each button.  The Bounce object
// automatically deals with contact chatter or "bounce", and
// it makes detecting changes very simple.
// 10 = 10ms debounce time which is appropriate for most
// mechanical pushbuttons.
Bounce button0 = Bounce(pin0, 10);
Bounce button1 = Bounce(pin1, 10);
Bounce button2 = Bounce(pin2, 10);


void setup() {
  pinMode(pin0, INPUT_PULLUP);
  pinMode(pin1, INPUT_PULLUP);
  pinMode(pin2, INPUT_PULLUP);
}

void loop() {
  // Update all the buttons.  There should not be any long
  // delays in loop(), so this runs repetitively at a rate
  // faster than the buttons could be pressed and released.
  button0.update();
  button1.update();
  button2.update();

  // Falling edge check.
  if (button0.fallingEdge()) {
    Keyboard.press(key0);
  }
  if (button1.fallingEdge()) {
    Keyboard.press(key1);
  }
  if (button2.fallingEdge()) {
    Keyboard.press(key2);
  }

  // Rising edge check.
  if (button0.risingEdge()) {
    Keyboard.release(key0);
  }
  if (button1.risingEdge()) {
    Keyboard.release(key1);
  }
  if (button2.risingEdge()) {
    Keyboard.release(key2);
  }
} <Bounce.h>

int pin0 = 7;
int pin1 = 12;
int pin2 = 14;

int key0 = KEY_ESC;
int key1 = KEY_Z;
int key2 = KEY_X;

// Create Bounce objects for each button.  The Bounce object
// automatically deals with contact chatter or "bounce", and
// it makes detecting changes very simple.
// 10 = 10ms debounce time which is appropriate for most
// mechanical pushbuttons.
Bounce button0 = Bounce(pin0, 10);
Bounce button1 = Bounce(pin1, 10);
Bounce button2 = Bounce(pin2, 10);


void setup() {
  pinMode(pin0, INPUT_PULLUP);
  pinMode(pin1, INPUT_PULLUP);
  pinMode(pin2, INPUT_PULLUP);
}

void loop() {
  // Update all the buttons.  There should not be any long
  // delays in loop(), so this runs repetitively at a rate
  // faster than the buttons could be pressed and released.
  button0.update();
  button1.update();
  button2.update();

  // Falling edge check.
  if (button0.fallingEdge()) {
    Keyboard.press(key0);
  }
  if (button1.fallingEdge()) {
    Keyboard.press(key1);
  }
  if (button2.fallingEdge()) {
    Keyboard.press(key2);
  }

  // Rising edge check.
  if (button0.risingEdge()) {
    Keyboard.release(key0);
  }
  if (button1.risingEdge()) {
    Keyboard.release(key1);
  }
  if (button2.risingEdge()) {
    Keyboard.release(key2);
  }
}
